var aleatorio=[];
var minumero=[];

var intentos = 0;

var audioPerder = new Audio('audio/lose.mp3');

var audioGanar = new Audio('audio/win.mp3');

window.onload = function(){
    
    //Cramos un numero aleatorio
    numueroAleatorio();
    console.log(aleatorio);

    /*-------------------------- BOTONES ARRIBA ---------------------------------------*/
    document.getElementsByName('uno')[0].onclick = function(){
        operacion(this.name,this.dataset.op);
    }
    document.getElementsByName('dos')[0].onclick = function(){
        operacion(this.name,this.dataset.op);
    }
    document.getElementsByName('tres')[0].onclick = function(){
        operacion(this.name,this.dataset.op);
    }
    document.getElementsByName('cuatro')[0].onclick = function(){
        operacion(this.name,this.dataset.op);

        //Desbloqueo el boton verificar clave
        var boton=document.querySelector("input[type ='button']");
        boton.disabled=false;
    }

    /*-------------------------- BOTONES ABAJO ---------------------------------------*/

    document.getElementsByName('uno')[2].onclick = function(){
        operacion(this.name,this.dataset.op);
    }
    document.getElementsByName('dos')[2].onclick = function(){
        operacion(this.name,this.dataset.op);
    }
    document.getElementsByName('tres')[2].onclick = function(){
        operacion(this.name,this.dataset.op);
    }
    document.getElementsByName('cuatro')[2].onclick = function(){
        operacion(this.name,this.dataset.op);
        
        //Desbloqueo el boton verificar clave
        var boton=document.querySelector("input[type ='button']");
        boton.disabled=false;
    }

    document.getElementById("verificar").addEventListener("click",comprobar);


}

/*--------------------- COMPROBAMOS SI TENEMOS QUE SUMAR O RESTAR ------------- */

function operacion(nombre, oper){
    if(oper == 'sumar'){
        sumarNumero(nombre);
    }else{
        restarNumero(nombre);
    }
}

/*--------------------------- SUMAMOS NUMERO (LO HACEMOS GRANDE) ------------- */

function sumarNumero(nombreInput){
    let input = document.querySelector('input[name='+nombreInput+']');
    
    let num = parseInt(input.value);

    let nuevoNum = num;

    //console.log(num);

    if(isNaN(num) || num == 9){
        nuevoNum = 0;
    }else{
        nuevoNum++
        ;
    }

    input.value = nuevoNum;
}

/*--------------------------- RESTAMOS NUMERO (LO HACEMOS PEQUEÑO) ------------- */

function restarNumero(nombreInput){
    let input = document.querySelector('input[name='+nombreInput+']');
    
    let num = parseInt(input.value);

    let nuevoNum = num;

    //console.log(num);

    if(isNaN(num) || num == 0){
        nuevoNum = 9;
    }else{
        nuevoNum--;
    }

    input.value = nuevoNum;
}

/*--------------------------- CREAMOS EL NUMERO ALEATORIO DE 4 DIGITOS ------------- */

function numueroAleatorio(){
    var cont = 1;
    while(cont < 5){
        var num = Math.floor(Math.random()*(9-0)+1);
        if(!comprobarRepetidos(num)){
            aleatorio.push(num);
            cont++;
        }
    }
}

function comprobarRepetidos(num){
    let tam = aleatorio.length;
    let esta = false;
    for(let i = 0; i < tam && !esta; i++){
        if(aleatorio[i] == num){
            esta = true;
        }
    }
    return esta;
}


/*--------------------- COMPROBAMOS SI EL NUMERO INTRODUCIDO ES CORRECTO------------- */

function comprobar(){
    let contenedor = document.querySelector('#intentosPrev');

    let parrIntentos = document.querySelector('#intentosNext p');

    console.log(parrIntentos);

    insertarDiv();

    var numVerdes = 0; //número de verdes

    if(intentos < 6){
        //comprueba y sigue jugando

        // 1) Sacamos el numero
        sacar_numero();

        // 2) Vemos si coincide en la misma posición o se encuentra dentro del numero seleccionado
        let tam = aleatorio.length;
        for(let i = 0; i < tam; i++){
            if(comprobarRepetidos(minumero[i])){ //vemos si está en el array
                //comprobamos si esta en la misma posi
                if(comprobarPosi(minumero[i],i)){ //está en la misma posi
                    console.log('verde');
                    numVerdes ++;
                    
                    let coso = crearInput(minumero[i],'colorVerde');
                    contenedor.lastChild.appendChild(coso);

                    if(numVerdes == 4){
                        console.log('HAS GANADO.');
                        let ruta = 'img/win.jpg';
                        crearPopup(ruta);
                        audioGanar.play();
                        audioGanar.loop = true;
                    }
                }else{
                    console.log('amarillo');
                    let coso = crearInput(minumero[i],'colorAmarillo');
                    contenedor.lastChild.appendChild(coso);
                }
            }else{
                //coloreamos en rojo
                console.log('rojo');
                let coso = crearInput(minumero[i],'colorRojo');
                contenedor.lastChild.appendChild(coso);
            }
        }
        intentos ++; //número de intentos
        parrIntentos.innerHTML = 'Intentos restantes: '+(6-intentos);
    }else{
        //ha perdido
        console.log('HAS PERDIDO.');
        let ruta = 'img/lose.jpg';
        crearPopup(ruta);
        audioPerder.play();
        audioPerder.loop = true;
    }

}

function crearInput(numero,clase){
    let input = document.createElement('input');
    
    input.setAttribute('type','text');

    input.setAttribute('readonly',true);

    input.setAttribute('class',clase);

    input.value = numero;

    return input;
}

function comprobarPosi(num,pos){
    let tam = aleatorio.length;
    let esta = false;
    for(let i = 0; i < tam && !esta; i++){
        if(aleatorio[i] == num && i == pos){    //son iguales y están en la misma posi
            esta = true;
        }
    }
    return esta;
}

function insertarDiv(){
    let caja = document.createElement('div');
    caja.setAttribute('class','cajaIntentos');

    let contenedor = document.querySelector('#intentosPrev');

    if(contenedor.childElementCount == 4){
        let primerDiv = document.querySelector('#intentosPrev div');
        contenedor.removeChild(primerDiv);
        contenedor.appendChild(caja);
    }else{
        contenedor.appendChild(caja);
    }
}



/*--------------------- VEMOS QUE NUMERO A COLOCADO NUESTRO USUARIO ------------- */

function sacar_numero(){

    //Vaciamos por si tiene algo
    minumero=[];

    let tam = 4;
    var contenidos=document.querySelectorAll("#intentosNext input[type ='text']");

    for (let i=0;i<tam;i++){
        //Lo añadimos a mi array
        minumero.push(contenidos[i].value);
    }

    console.log(minumero);
}

/*---------------------------------------------CREAMOS EL POP UP QUE SALE AL ACABAR LA PARTIDA-------------------------------------------------*/

function crearPopup(action){

	//Creamos el mensaje que sale después de perder o ganar
	var ventana = document.createElement('div');
	ventana.setAttribute('onclick','borrar()');
	ventana.style.backgroundImage = 'url('+action+')';
	ventana.setAttribute('id','capa');
	document.body.appendChild(ventana);
}

//La siguiente función borra la capa creada 

function borrar(){
	var capa = document.querySelector('#capa');

	document.body.removeChild(capa);

	location.reload(); //reiniciamos todo

}
